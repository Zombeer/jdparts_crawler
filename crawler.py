import requests
from lxml import etree
import re
from io import StringIO
from pymongo import MongoClient
from multiprocessing import Pool

class JD_sess(object):
    client = MongoClient('localhost')
    db = client.john_deere
    coll = db.parts
    sess = requests.Session()
    model_name = ''
    section_name = ''
    illustration_name = ''
    model_num = ''
    section_prefix = ''

    def begin(self):
        print('Getting home page ... ', end='')
        self.sess.get('http://jdpc.deere.com/jdpc/servlet/com.deere.u90490.partscatalog.view.servlets.HomePageServlet_Alt?Origin=JDParts')
        print('Done. Session initiated.')

    def get_cat_number(self, number):
        self.model_num = number
        print('Requesting catalog# {} ... '.format(number), end='')
        self.sess.get('http://jdpc.deere.com/jdpc/servlet/com.deere.u90490.partscatalog.view.servlets.CatalogSearchServlet?catalog_no={}&grid_no=&section_no=&page_no='.format(number))
        print('Done.')

    def get_sections(self):
        # Getting top page of catalog
        print('Getting catalog top page ... ', end='')
        resp = self.sess.get('http://jdpc.deere.com/jdpc/servlet/com.deere.u90490.partscatalog.view.servlets.LeftNavServlet').text
        print('Done')
        response = etree.HTML(resp)
        link = response.xpath('//a[1]')[0].get('href')
        print('Hitting main +  ... ', end='')
        resp = self.sess.get(link).text
        response = etree.HTML(resp)
        print('Done')
        try:
            link = response.xpath('//a[text()="Sectional Index"]')[0].get('href')
            self.section_prefix = response.xpath('//a[text()="Sectional Index"]/parent::td/a')[0].get('name')
            self.model_name =  response.xpath('//a[@class="navHighlight"]')[0].text

        except:
            print('No links found..')
            return []

        print('Getting section urls  ... ', end='')
        resp = self.sess.get(link).text
        response = etree.HTML(resp)
        print('Done')
        section_data = [(x.get('href'), x.text) for x in response.xpath('//a[contains(@name, "{}:")]/following-sibling::a'.format(self.section_prefix))]
        return section_data

    def get_illustrations(self, section_data):
        section_url, section_name = section_data
        self.section_name = section_name
        print('Getting illustration urls ... ', end='')
        resp = self.sess.get(section_url).text
        print('Done')
        response = etree.HTML(resp)
        illustration_data = [(x.get('onclick').replace("return doPage('" ,'').replace("');", ''), x.text) for x in response.xpath('//a[re:test(@name, "{}:\d+:\d+")]/following-sibling::a'.format(self.section_prefix) , namespaces={'re': "http://exslt.org/regular-expressions"})]
        print('illustration_data: {}'.format(illustration_data))
        return illustration_data

    def get_parts(self, illustration_data):
        illustration_params, illustration_name = illustration_data
        url = 'http://jdpc.deere.com/jdpc/servlet/com.deere.u90490.partscatalog.view.servlets.KeyLineServlet{}'.format(illustration_params)
        print('Parts table url: {}'.format(url))
        print('Getting parts list ... ', end='')
        resp = self.sess.get(url).text
        print('Done')
        print('Getting illustration ... ', end='')
        illustration_resp = self.sess.get('http://jdpc.deere.com/jdpc/servlet/com.deere.u90490.partscatalog.view.servlets.ImagePageServlet?page=2').text
        print('Done')
        illustration_response = etree.HTML(illustration_resp)
        try:
            illustration_url = illustration_response.xpath('//img')[0].get('src')
        except:
            illustration_url = 'N/A'
        print(illustration_url)

        response = etree.HTML(resp)
        trs = response.xpath('//tr')
        for tr in trs:
            item = {}
            tds = tr.xpath('./td[@class="defaultCell"]')
            if not tds:
                continue
            item['key'] = tds[1].text
            item['part_number'] = tds[2].text
            item['part_name'] = tds[3].text
            item['qty'] = tds[4].text
            item['remarks'] = tds[6].text
            item['illustration_url'] = illustration_url
            item['section_name'] = self.section_name
            item['model_num'] = self.model_num
            item['model_name'] = self.model_name
            item['illustration_name'] = illustration_name
            print(item)
            self.coll.insert_one(item)

    def get_parts_by_model(self, number):
        self.get_cat_number(number)
        section_data = self.get_sections()
        for section in section_data:
            illustration_data = self.get_illustrations(section)
            for illustration in illustration_data:
                self.get_parts(illustration)
        try:
            if illustration_data:
                with open('scraped_models.txt', 'a') as f:
                    f.write(str(number)+'\n')
        except:
            pass
        return

def worker(numbers):
    jd = JD_sess()
    jd.begin()
    for num in numbers:
        jd.get_parts_by_model(num)

def chunks(l, n):
    length = int(len(l)/n)
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), length):
        yield l[i:i + length]

pool = Pool(10)
pool.map(worker, chunks(range(100,500), 10))
